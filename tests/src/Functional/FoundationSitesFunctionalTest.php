<?php

namespace Drupal\Tests\foundation_sites\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group foundation_sites
 */
class FoundationSitesFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'foundation_sites',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-foundation-sites');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  // public function testAccessSettingsPage() {
  //   $session = $this->assertSession();
  //   // Go to settings page as an admin:
  //   $this->drupalGet('/admin/config/user-interface/foundation-sites');
  //   $session->statusCodeEquals(200);
  //   // Go to settings page as an authenticated user with the right permission:
  //   $this->drupalLogout();
  //   $userWithPerm = $this->drupalCreateUser(['access foundation_sites settings page']);
  //   $this->drupalLogin($userWithPerm);
  //   $this->drupalGet('/admin/config/user-interface/foundation-sites');
  //   $session->statusCodeEquals(200);
  //   // Go to settings page as an authenticated user without permission:
  //   $this->drupalLogout();
  //   $this->drupalLogin($this->user);
  //   $this->drupalGet('/admin/config/user-interface/foundation-sites');
  //   $session->statusCodeEquals(403);
  //   // Go to settings page as an anonymous user:
  //   $this->drupalLogout();
  //   $this->drupalGet('/admin/config/user-interface/foundation-sites');
  //   $session->statusCodeEquals(403);
  // }

}
