<?php

// namespace Drupal\foundation_sites\Form;
//
// use Drupal\Core\Extension\ThemeHandler;
// use Drupal\Core\Form\ConfigFormBase;
// use Drupal\Core\Form\FormStateInterface;
// use GuzzleHttp\Exception\RequestException;
// use Symfony\Component\DependencyInjection\ContainerInterface;
//
// /**
//  * Configure foundation_sites settings for this site.
//  */
// class ZurbFoundationLibrarySettingsForm extends ConfigFormBase {
//
//   /**
//    * The theme handler service.
//    *
//    * @var \Drupal\Core\Extension\ThemeHandler
//    */
//   protected $themeHandler;
//
//   /**
//    * {@inheritDoc}
//    */
//   public function __construct(ThemeHandler $themeHandler) {
//     $this->themeHandler = $themeHandler;
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public static function create(ContainerInterface $container) {
//     return new static(
//       $container->get('theme_handler'),
//     );
//   }
//
//   /**
//    * {@inheritDoc}
//    */
//   public function getFormId() {
//     return 'foundation_sites_admin_settings';
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   protected function getEditableConfigNames() {
//     return [
//       'foundation_sites.settings',
//     ];
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public function buildForm(array $form, FormStateInterface $form_state) {
//     $config = $this->config('foundation_sites.settings');
//
//     $themes = $this->themeHandler->listInfo();
//     $active_themes = [];
//     foreach ($themes as $key => $theme) {
//       if ($theme->status) {
//         $active_themes[$key] = $theme->info['name'];
//       }
//     }
//     // Load from CDN.
//     $form['cdn'] = [
//       '#type' => 'fieldset',
//       '#title' => $this->t('Load ZURB Foundation from CDN'),
//       '#collapsible' => TRUE,
//       '#collapsed' => TRUE,
//     ];
//     $data = _foundation_sites_data();
//     $cdn_options_array = json_decode($data, TRUE);
//     $versions = array_keys($cdn_options_array['zurb_foundation']);
//     $form['cdn']['zurb_foundation'] = [
//       '#type' => 'select',
//       '#title' => $this->t('Select ZURB Foundation version to load via CDN, non for local library'),
//       '#options' => str_replace('_', '.', $versions),
//       '#empty_option' => $this->t('-None-'),
//       '#default_value' => !empty($config->get('cdn.zurb_foundation_version')) ? str_replace('_', '.', $config->get('cdn.zurb_foundation_version')) : '',
//     ];
//     // Production or minimized version.
//     $form['minimized'] = [
//       '#type' => 'fieldset',
//       '#title' => $this->t('Minimized, Non-minimized, or Composer version'),
//       '#collapsible' => TRUE,
//       '#collapsed' => FALSE,
//     ];
//     $form['minimized']['minimized_options'] = [
//       '#type' => 'radios',
//       '#title' => $this->t('Choose minimized, non-minimized, or composer version.'),
//       '#options' => [
//         0 => $this->t('Use non minimized libraries (Development)'),
//         1 => $this->t('Use minimized libraries (Production)'),
//         2 => $this->t('Use composer installed libraries'),
//       ],
//       '#default_value' => $config->get('minimized_option'),
//     ];
//     // Per-theme visibility.
//     $form['theme'] = [
//       '#type' => 'fieldset',
//       '#title' => $this->t('Themes Visibility'),
//       '#collapsible' => TRUE,
//       '#collapsed' => FALSE,
//     ];
//     $form['theme']['theme_visibility'] = [
//       '#type' => 'radios',
//       '#title' => $this->t('Activate on specific themes'),
//       '#options' => [
//         0 => $this->t('All themes except those listed'),
//         1 => $this->t('Only the listed themes'),
//       ],
//       '#default_value' => $config->get('theme.visibility'),
//     ];
//     $form['theme']['theme_themes'] = [
//       '#type' => 'select',
//       '#title' => 'List of themes where library will be loaded.',
//       '#options' => $active_themes,
//       '#multiple' => TRUE,
//       '#default_value' => $config->get('theme.themes'),
//       '#description' => $this->t("Specify in which themes you wish the library to load."),
//     ];
//     // Per-path visibility.
//     $form['url'] = [
//       '#type' => 'fieldset',
//       '#title' => $this->t('Activate on specific URLs'),
//       '#collapsible' => TRUE,
//       '#collapsed' => TRUE,
//     ];
//     $form['url']['url_visibility'] = [
//       '#type' => 'radios',
//       '#title' => $this->t('Load zurb_foundation on specific pages'),
//       '#options' => [
//         0 => $this->t('All pages except those listed'),
//         1 => $this->t('Only the listed pages'),
//       ],
//       '#default_value' => $config->get('url.visibility'),
//     ];
//     $form['url']['url_pages'] = [
//       '#type' => 'textarea',
//       '#title' => '<span class="element-invisible">' . $this->t('Pages') . '</span>',
//       '#default_value' => !empty($config->get('url.pages')) ? implode('\n', $config->get('url.pages')) : '',
//       '#description' => $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
//         '%blog' => 'blog',
//         '%blog-wildcard' => 'blog/*',
//         '%front' => '<front>',
//       ]),
//     ];
//
//     // Files settings.
//     $form['files'] = [
//       '#type' => 'fieldset',
//       '#title' => $this->t('Files Settings'),
//       '#collapsible' => TRUE,
//       '#collapsed' => TRUE,
//     ];
//     $form['files']['types'] = [
//       '#type' => 'checkboxes',
//       '#title' => $this->t('Select which files to load from the library. By default you should check both, but in some cases you will need to load only CSS or JS zurb_foundation files.'),
//       '#options' => [
//         'css' => $this->t('CSS files'),
//         'js' => $this->t('Javascript files'),
//       ],
//       '#default_value' => $config->get('file_types') ?? [],
//     ];
//
//     return parent::buildForm($form, $form_state);
//   }
//
//   /**
//    * {@inheritdoc}
//    */
//   public function submitForm(array &$form, FormStateInterface $form_state) {
//     $this->config('foundation_sites.settings')
//       ->set('theme.visibility', $form_state->getValue('theme_visibility'))
//       ->set('theme.themes', $form_state->getValue('theme_themes'))
//       ->set('url.visibility', $form_state->getValue('url_visibility'))
//       ->set('url.pages', explode('\n', $form_state->getValue('url_pages')))
//       ->set('minimized_option', $form_state->getValue('minimized_options'))
//       ->set('cdn.zurb_foundation_version', str_replace('.', '_', $form_state->getValue('zurb_foundation')))
//       ->set('file_types', $form_state->getValue('types'))
//       ->save();
//     parent::submitForm($form, $form_state);
//   }
//
// }
//
// /**
//  * Replaces CDN version options.
//  *
//  * This is for finding uptodate solutions for CDN Version options.
//  */
// function _foundation_sites_data() {
//   return str_replace('.', '_', '{
//   "timestamp": "2015-11-09T18:54:50.335Z",
//   "zurb_foundation": {
//     "4.1.1": {
//       "css": "//stackpath.zurb_foundationcdn.com/zurb_foundation/4.1.1/css/zurb_foundation.min.css",
//       "js": [
//         "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js",
//         "//stackpath.zurb_foundationcdn.com/zurb_foundation/4.1.1/js/zurb_foundation.min.js"
//       ]
//     },
//     "4.0.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/4.0.0/css/zurb_foundation.min.css",
//       "js": [
//         "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js",
//         "//maxcdn.zurb_foundationcdn.com/zurb_foundation/4.0.0/js/zurb_foundation.min.js"
//       ]
//     },
//     "3.3.7": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.7/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.7/js/zurb_foundation.min.js"
//     },
//     "3.3.6": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.6/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.6/js/zurb_foundation.min.js"
//     },
//     "3.3.5": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.5/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.5/js/zurb_foundation.min.js"
//     },
//     "3.3.4": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.4/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.4/js/zurb_foundation.min.js"
//     },
//     "3.3.3": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.3/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.3/js/zurb_foundation.min.js"
//     },
//     "3.3.2": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.2/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.2/js/zurb_foundation.min.js"
//     },
//     "3.3.1": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.1/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.1/js/zurb_foundation.min.js"
//     },
//     "3.3.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.0/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.3.0/js/zurb_foundation.min.js"
//     },
//     "3.2.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.2.0/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.2.0/js/zurb_foundation.min.js"
//     },
//     "3.1.1": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.1.1/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.1.1/js/zurb_foundation.min.js"
//     },
//     "3.1.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.1.0/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.1.0/js/zurb_foundation.min.js"
//     },
//     "3.0.3": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.3/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.3/js/zurb_foundation.min.js"
//     },
//     "3.0.2": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.2/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.2/js/zurb_foundation.min.js"
//     },
//     "3.0.1": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.1/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.1/js/zurb_foundation.min.js"
//     },
//     "3.0.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.0/css/zurb_foundation.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.0/js/zurb_foundation.min.js"
//     },
//     "3.0.0-noicons": {
//       "css": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.0/css/zurb_foundation.no-icons.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/zurb_foundation/3.0.0/js/zurb_foundation.min.js"
//     },
//     "2.3.2": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.2/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.2/js/zurb_foundation.min.js"
//     },
//     "2.3.2-noicons": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.2/css/zurb_foundation-combined.no-icons.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.2/js/zurb_foundation.min.js"
//     },
//     "2.3.1": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.1/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.1/js/zurb_foundation.min.js"
//     },
//     "2.3.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.0/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.3.0/js/zurb_foundation.min.js"
//     },
//     "2.2.2": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.2/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.2/js/zurb_foundation.min.js"
//     },
//     "2.2.2-noresponsible": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.2/css/zurb_foundation.min.nr.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.2/js/zurb_foundation.min.js"
//     },
//
//     "2.2.1": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.1/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.1/js/zurb_foundation.min.js"
//     },
//     "2.2.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.0/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.2.0/js/zurb_foundation.min.js"
//     },
//     "2.1.1": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.1.1/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.1.1/js/zurb_foundation.min.js"
//     },
//     "2.1.0": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.1.0/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.1.0/js/zurb_foundation.min.js"
//     },
//     "2.0.4": {
//       "css": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.0.4/css/zurb_foundation-combined.min.css",
//       "js": "//maxcdn.zurb_foundationcdn.com/twitter-zurb_foundation/2.0.4/js/zurb_foundation.min.js"
//     }
//   },
//   "fontawesome": {
//     "4.4.0": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.4.0/css/font-awesome.min.css",
//     "4.2.0": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.2.0/css/font-awesome.min.css",
//     "4.1.0": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.1.0/css/font-awesome.min.css",
//     "4.0.3": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.0.3/css/font-awesome.min.css",
//     "4.0.2": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.0.2/css/font-awesome.min.css",
//     "4.0.1": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.0.1/css/font-awesome.min.css",
//     "4.0.0": "//maxcdn.zurb_foundationcdn.com/font-awesome/4.0.0/css/font-awesome.min.css",
//     "3.2.1": "//maxcdn.zurb_foundationcdn.com/font-awesome/3.2.1/css/font-awesome.min.css"
//   }
// }');
// }
//
// /**
//  * Load CDN version optios.
//  */
// function _foundation_sites_cdn_versions() {
//   static $uri = 'http://netdna.zurb_foundationcdn.com/data/zurb_foundationcdn.json';
//   try {
//     $client = \Drupal::httpClient();
//     $response = $client->get($uri, ['headers' => ['Accept' => 'text/plain']]);
//     $data = (string) $response->getBody();
//     if (empty($data)) {
//       return FALSE;
//     }
//   }
//   catch (RequestException $e) {
//     watchdog_exception('foundation_sites', $e);
//     return FALSE;
//   }
//   return $data;
// }
